package test

import (
	"fmt"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
)

func TestInstanceAWS(t *testing.T) {
	t.Parallel()
	testRegions := []string{"sa-east-1"}
	awsRegion := aws.GetRandomRegion(t, testRegions, nil)

	terraformOptions := &terraform.Options{
		TerraformDir: "../terraform",

		Vars: map[string]interface{}{
			"region": awsRegion,
		},

		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	publicIp := terraform.Output(t, terraformOptions, "ip_address")

	url := fmt.Sprintf("http://%s", publicIp)
	http_helper.HttpGetWithRetry(t, url, nil, 200, "Welcome nginx! - v1.0.0", 40, 5*time.Second)
}
