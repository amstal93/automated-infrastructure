FROM ubuntu:latest

ARG TERRAFORM_VERSION="0.12.23"
ARG ANSIBLE_VERSION="2.5.1"
ARG PACKER_VERSION="1.5.4"
ARG AWSCLI_VERSION="1.18.19"

LABEL maintainer="Leandro Moro <leandromoro1008@gmail.com>"
LABEL terraform_version=${TERRAFORM_VERSION}
LABEL ansible_version=${ANSIBLE_VERSION}
LABEL aws_cli_version=${AWSCLI_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
ENV AWSCLI_VERSION=${AWSCLI_VERSION}
ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}
RUN apt-get update \
    && apt-get install -y jq \
    && apt-get install -y golang \
    && apt-get install -y curl python3 python3-pip python3-boto unzip  \
    && pip3 install --upgrade awscli==${AWSCLI_VERSION} \
    && curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip '*.zip' -d /usr/local/bin \
    && rm *.zip \
    && apt-get clean \
    && rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/bin/bash"]